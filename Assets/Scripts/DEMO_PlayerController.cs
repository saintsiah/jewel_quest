﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DEMO_PlayerController : MonoBehaviour
{
    //public variables
    public Vector3 playerStartPosition;
    public float movementSpeed = 10;
    public DEMO_PickupSpawner pickupSpawner;
    public Pickup_Spawner pickupSpawner2;
    public TextMeshProUGUI pickupCountText;
    public GameObject winScreen;
    public GameObject pickupEffect;
    public AudioClip pickupSound;
    public AudioClip winSound;
    public float jumph;
    public float jumpforce;

    //Private variables
    private int spawnedPickupCount;
    private Rigidbody _rb;
    private int count = 0;
    private Vector3 jump;

    private bool allowMovement = false;
    private bool gameHasStarted = false;

    // Start is called before the first frame update
    void Start()
    {
        count = 0;
        //Get the Rigidbody of the player and cache it for use throughout
        _rb = gameObject.GetComponent<Rigidbody>();

        jump = new Vector3(0f, jumph, 0f);
        _rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        //If the game has started, start tracking our pickups
        if (gameHasStarted)
        {
            //If our count is greater than/equal to the total amount of pickups spawned, do something
            if (count >= spawnedPickupCount)
            {
                //Call our win condition
                OnWin();
            }
        }

        if (_rb.velocity.y == 0)
        {
            if(Input.GetKeyDown(KeyCode.Space))
            {
                _rb.AddForce(jump * jumpforce, ForceMode.Impulse);
            }
        }
    }

    //Sets the variables we need to set true at the start of the game
    public void GameHasStarted()
    {
        //Figure out how many pickups there are
        spawnedPickupCount = pickupSpawner.spawnedPickups.Count + pickupSpawner2.spawnedPickups.Count;
        gameHasStarted = true;
    }

    //This allows the character to move around
    public void ToggleCharacterMovement(bool _bool)
    {
        allowMovement = _bool;
        //If the rigidbody is set to kinematic, we can manually alter the transforms, if not, only physics can
        _rb.isKinematic = !_bool;
    }

    //All the variables we need to reset on reset
    public void OnGameReset()
    {
        //Turn our character movement off
        ToggleCharacterMovement(false);
        gameHasStarted = false;
        //reset our count to 0
        count = 0;
        //Reset the pickup available count
        spawnedPickupCount = 0;
    }

    //FixedUpdate is called on a fixed time scale, it's important to do all physics calculations here
    //so they happen at equal intervals 
    void FixedUpdate()
    {
        if (allowMovement)
        {
            //Get our input manager's inputs
            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical");

            //The amount we'll move based on our inputs
            Vector3 movement = new Vector3(moveHorizontal, 0, moveVertical);

            //Multiple the movement input by our speed and that amount of force to our player
            _rb.AddForce(movement * movementSpeed);
        }
    }

    //Custom method that calls all of our win conditions
    //We could put this all in the Update but this keeps the code cleaner
    private void OnWin()
    {
        //Turn our character movement off
        ToggleCharacterMovement(false);
        //Allow us to physically move the transform
        _rb.isKinematic = true;
        //Set the player back to their starting position
        gameObject.transform.position = playerStartPosition;
        //Write to our console that we won!
        Debug.Log("You Win!");
        //Turn on the end screen
        winScreen.SetActive(true);
        AudioSource.PlayClipAtPoint(winSound, transform.position);
    }

    //Whenever our player collides with a collider marked as a trigger, this will be called
    private void OnTriggerEnter(Collider other)
    {
        //If the other gameObject we hit has a tag of "Collectible," do something
        if (other.gameObject.CompareTag("Emerald"))
        {
            //Increase the pickup count
            count += 1;
            //Update our text to reflect how many pickups we collected
            pickupCountText.text = "$ " + count.ToString();
            //Play our particle system where the pickup was
            Instantiate(pickupEffect, other.transform.position, Quaternion.identity);
            AudioSource.PlayClipAtPoint(pickupSound, transform.position);
            //Destroy the pickup
            Destroy(other.gameObject);

            Debug.Log("count" + "/" + spawnedPickupCount);
        }

        if (other.gameObject.CompareTag("Pickup"))
        {
            //Increase the pickup count
            count += 2;
            //Update our text to reflect how many pickups we collected
            pickupCountText.text = "$ " + count.ToString();
            //Play our particle system where the pickup was
            Instantiate(pickupEffect, other.transform.position, Quaternion.identity);
            AudioSource.PlayClipAtPoint(pickupSound, transform.position);
            //Destroy the pickup
            Destroy(other.gameObject);

            Debug.Log("count" + "/" + spawnedPickupCount);
        }
    }
}
