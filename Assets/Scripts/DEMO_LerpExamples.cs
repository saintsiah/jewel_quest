﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DEMO_LerpExamples : MonoBehaviour
{
    public Transform startPosition, endPosition;
    public float lerpDuration;
    public float waitTime;
    public float waitTimeMax, waitTimeMin;
    public AnimationCurve lerpSpeed;

    private bool hasLerped = false;
    private bool canLerp = true;
    private bool playEvent = true;

    Coroutine m_movementRoutineRef;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ControllingCubeMovement());
    }

    // Update is called once per frame
    void Update()
    {
        //The input keys test different lerp methods
        if (Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log("A hit");
            StartCoroutine(LerpCube(endPosition.position, lerpDuration));
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            Debug.Log("S hit");
            StartCoroutine(SmoothStepCube(endPosition.position, lerpDuration));
        }

        if (Input.GetKeyDown(KeyCode.T))
        {
            Debug.Log("T hit");
            ToggleCubePosition();
        }
    }

    //This runs independent of frame rate so we can get a smoother animation
    private void FixedUpdate()
    {
        //If playEvent is true do stuff
        if (playEvent)
        {
            //Set our new coroutine to our variable so we have a reference and can end it if we need to
            m_movementRoutineRef = StartCoroutine(ControllingCubeMovement());
            //Set playEvent false so we only have one live right now
            playEvent = false;
        }
    }

    //This is calling the cube to lerp
    public void ToggleCubePosition()
    {
        //Can we lerp?
        if (canLerp)
        {
            //if Yes have we lerped already? if so
            if (!hasLerped)
            {
                hasLerped = true;
                StartCoroutine(SmoothStepCube(endPosition.position, lerpDuration));
            }
            else
            {
                hasLerped = false;
                StartCoroutine(SmoothStepCube(startPosition.position, lerpDuration));
            }
        }
    }

    //This coroutine will constantly call ToggleCubePosition() until the object dies
    IEnumerator ControllingCubeMovement()
    {
        //Toggle our position
        ToggleCubePosition();
        //Wait until the lerp animation is done
        yield return new WaitForSeconds(lerpDuration);
        //Wait until next frame
        yield return null;
        //determine our wait time and wait
        var _waitTime = Random.Range(waitTimeMin, waitTimeMax);
        yield return new WaitForSeconds(_waitTime);
        //Wait until next frame
        yield return null;
        //Start the next coroutine
        //You could nest the coroutine here and call StartCoroutine(ConrollingCubeMovement) but we currently have our lerp tied to
        //Fixed Updated so we call this instead
        playEvent = true;
        //End this coroutine
        yield break;
    }

    //This is the coroutine doing the actual lerping!
    IEnumerator LerpCube(Vector3 _endPos, float _duration)
    {
        //Prevent us from lerping while we're currently lerping
        canLerp = false;
        //We need to track our elapsed time for the lerp
        float elapsedTime = 0;
        //Get our current position
        var startPos = transform.position;
        //Make a variable for where we're going
        //This vector3 short hand is = Vector3 (0,0,0)
        var targetPos = Vector3.zero;
        //While our elapsed time is less than our duration, we do stuff
        while (elapsedTime <= _duration)
        {
            //We only want to move on the x axis so we lerp our x position from our start to end postion based on
            //the percentage of elapsed time / duration
            targetPos.x = Mathf.LerpUnclamped(startPos.x, _endPos.x, elapsedTime / _duration);
            //wait until next frame
            elapsedTime += Time.deltaTime;
            //Move our cube by setting it's new position
            transform.position = targetPos;
        }
        //We can now lerp again!
        canLerp = true;
        //End this coroutine
        yield break;
    }

    //Smooth step is a math function that returns a value between 0 - 1 based on a pct value similar to a lerp,
    //only the values ease in and out without the need for an easing function. Same exact setup and workings as our lerp function
    //just using Mathf.Smoothstep instead of Mathf.Lerp
    IEnumerator SmoothStepCube(Vector3 _endPos, float duration)
    {
        canLerp = false;
        float elapsedTime = 0;
        var startPos = transform.position;

        Vector3 targetPos = new Vector3(0, 0, 0);

        while (elapsedTime <= duration)
        {
            targetPos.x = Mathf.SmoothStep(startPos.x, _endPos.x, elapsedTime / duration);
            yield return null;

            elapsedTime += Time.deltaTime;

            transform.position = targetPos;
        }
        canLerp = true;
        yield break;
    }


    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Lerp functions ~~~~~~~~~~~~~~~~~~~~~~~~~~~

    //This is the Unity Mathf.Lerp function
    static float VELerp(float _startValue, float _endValue, float pct)
    {
        pct = Mathf.Clamp01(pct);
        return _startValue + (_endValue - _startValue) * pct;
    }

    //This is the Unity Mathf.LerpUnclamped function. The only difference is the percent value can go above 1
    static float VELerpUnclampped(float _startValue, float _endValue, float pct)
    {
        return _startValue + (_endValue - _startValue) * pct;
    }

    //This function gets the inverse value of a float
    public static float Flip(float t)
    {
        return 1 - t;
    }

    //Below are various easing functions you can use to manipulate a lerps "pct" value. For more information
    //visit the resources posted in the Discord
    public static float EaseIn(float t)
    {
        return t * t;
    }

    public static float EaseInQuadratic(float t)
    {
        return t * t * t * t;
    }

    public static float EaseOut(float t)
    {
        return Flip(Mathf.Sqrt(Flip(t)));
    }

    public static float EaseInOut(float t)
    {
        return Mathf.Lerp(EaseIn(t), EaseOut(t), t);
    }
}
