﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Pickup_Spawner : MonoBehaviour
{
    //Public variables
    public GameObject pickupPrefab;
    public Transform pickupParent;
    public List<Transform> spawnLocations;
    public List<GameObject> spawnedPickups;

    public UnityEvent pickupsSpawned;

    //This tag lets us put so many pixels of space between our variables in the inspector
    [Space(10)]
    public bool _debug;

    //Awake is called even before start
    private void Awake()
    {

    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    //This method handles all of the spawning
    public void SpawnPickups()
    {
        //If we are debugging, do this, if not, who cares
        if (_debug)
        {
            //This instantiates all the pickups the same way as our regular method, but we only do one so we don't have to collect as many
            GameObject go = Instantiate(pickupPrefab);
            go.transform.position = spawnLocations[0].position;
            go.transform.parent = pickupParent;
            spawnedPickups.Add(go);
            pickupsSpawned.Invoke();
        }
        else
        {
            //For each transform we added to our spawnLocations list, we will cycle through and do something with it
            foreach (Transform _spawnPoint in spawnLocations)
            {
                //This is the gameObject we are going to spawn
                GameObject go = Instantiate(pickupPrefab);
                //Set the position of the pickup to the position of the spawn point
                go.transform.position = _spawnPoint.position;
                //Set the parent of the pickup to keep our hierarchy clean
                go.transform.parent = pickupParent;
                //Add the pickup to a list of all the pickups that were spawned
                spawnedPickups.Add(go);
                pickupsSpawned.Invoke();
            }
        }
    }

    //Incase we need to destroy all of our pickups
    public void DestroyPickups()
    {
        //For each GameObject we added to our spawnedPickups list, we will cycle through and do something with it
        foreach (GameObject _pickup in spawnedPickups)
        {
            //Destroy the pickup
            Destroy(_pickup);
        }

        //Clear our spawnedPickups list since we destroyed them all
        spawnedPickups.Clear();
    }
}

